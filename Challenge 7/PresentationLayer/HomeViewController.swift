//
//  HomeViewController.swift
//  Challenge 7
//
//  Created by Tatang Sulaeman on 04/08/22.
//

import UIKit
import CoreData

class HomeViewController: UIViewController {

    var image: UIImage = UIImage()
    var videoURL: URL = URL(fileURLWithPath: "")
    func createFormItem(name: String, longValue: Bool = false) -> UIStackView{
        lazy var label: UILabel = {
            let lbl = UILabel()
            lbl.text = name
            lbl.textColor = .black
            lbl.font = UIFont(name:"HelveticaNeue-bold", size: 15)
            return lbl
        }()
        
        lazy var textField: UITextField = {
            let txtField = UITextField()
            txtField.layer.cornerRadius = 5
            txtField.layer.borderColor = UIColor.systemGray.cgColor
            txtField.layer.borderWidth = 1
            txtField.font = UIFont.systemFont(ofSize: 15)
            let padding = UIView(frame: CGRect(x: 0.0, y: 0.0, width: 10, height: 40))
            txtField.leftView = padding
            txtField.leftViewMode = .always
            txtField.rightView = padding
            txtField.rightViewMode = .always
            txtField.addConstraint(txtField.heightAnchor.constraint(equalToConstant: 40))
            return txtField
        }()
        
        lazy var textView: UITextView = {
            let txtView = UITextView()
            txtView.layer.cornerRadius = 5
            txtView.layer.borderColor = UIColor.systemGray.cgColor
            txtView.layer.borderWidth = 1
            txtView.font = UIFont.systemFont(ofSize: 15)
            txtView.addConstraint(txtView.heightAnchor.constraint(equalToConstant: 100))

            return txtView
        }()
                               
        
        lazy var formItemStack: UIStackView = {
            let stackView = UIStackView()
            stackView.addArrangedSubview(label)
            if (longValue == true){
                stackView.addArrangedSubview(textView)
            }
            else{
                stackView.addArrangedSubview(textField)
            }
           

            stackView.axis = .vertical
            stackView.translatesAutoresizingMaskIntoConstraints = false
            return stackView
        }()
        
        return formItemStack
    }
    
    lazy var nameFormStack: UIStackView = createFormItem(name:"Resto name")
    lazy var ratingFormStack: UIStackView = createFormItem(name:"Rating")
    lazy var descFormStack: UIStackView = createFormItem(name:"Description", longValue: true)
    lazy var favMenuFormStack: UIStackView = createFormItem(name:"Favorite Menu")
    
    private lazy var addImageButton: UIButton = {
        let button = UIButton()
        button.setTitle("  add Image  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(navigateToUploadImage), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var addVideoButton: UIButton = {
        let button = UIButton()
        button.setTitle("  add Video  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(navigateToUploadVideo), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var addFileStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [addImageButton,UIView(),addVideoButton])
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    private lazy var sendReviewButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Send Review  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(sendReview), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    lazy var formStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            nameFormStack,
            ratingFormStack,
            descFormStack,
            favMenuFormStack
        ])
        stackView.spacing = 10
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
     }()
    
    
    @objc func navigateToUploadVideo(){
        let uploadVideoVC = UploadVideoViewController()
        uploadVideoVC.delegate = self
        self.navigationController?.pushViewController(uploadVideoVC, animated: true)
    }
    
    @objc func navigateToUploadImage(){
        let uploadImageVC = UploadImageViewController()
        uploadImageVC.delegate = self
        self.navigationController?.pushViewController(uploadImageVC, animated: true)
    }
    
    @objc func sendReview(){
        let fieldName = nameFormStack.arrangedSubviews[1] as! UITextField
        let fieldRating = ratingFormStack.arrangedSubviews[1] as! UITextField
        let fieldDesc = descFormStack.arrangedSubviews[1] as! UITextView
        let fieldFavMenu = favMenuFormStack.arrangedSubviews[1] as! UITextField
        
        if (fieldName.text == "" || fieldRating.text == "" || fieldDesc.text == "" || fieldFavMenu.text == "" ){
            let alert = UIAlertController(title: "Alert", message: "please fill all fields", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
            return
        }
        
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let managedContext = appDelegate.persistentContainer.viewContext
        let userEntity = ReviewEntity(context: managedContext)
        
        userEntity.name = fieldName.text
        userEntity.rating = fieldRating.text
        userEntity.desc = fieldDesc.text
        userEntity.fav_menu = fieldFavMenu.text
        userEntity.image = image.jpegData(compressionQuality: 0.5)
        userEntity.video = NSData(contentsOf:videoURL) as Data?
        
        do {
            try managedContext.save()
        } catch {
            print("Error")
        }
        
        let reviewVC = ReviewViewController()
        self.navigationController?.pushViewController(reviewVC, animated: true)
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home"
    
        view.backgroundColor = .white
        setupLayout()
    }
    
    func setupLayout(){
        view.addSubview(formStack)
        view.addSubview(addFileStack)
        view.addSubview(sendReviewButton)
        NSLayoutConstraint.activate([
            formStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            formStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            formStack.topAnchor.constraint(equalTo: view.topAnchor,constant: 100),
            
            addFileStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            addFileStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            addFileStack.topAnchor.constraint(equalTo: formStack.bottomAnchor,constant: 50),
            
            
            sendReviewButton.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            sendReviewButton.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            sendReviewButton.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -50),

        ])
        
    }

}
