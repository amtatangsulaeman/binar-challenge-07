//
//  UIImageView+Extension.swift
//  Challenge 7
//
//  Created by Tatang Sulaeman on 04/08/22.
//

import Foundation
import UIKit

extension UIImage {
    
    func resizeImageTo(size: CGSize) -> UIImage? {
        return UIGraphicsImageRenderer(size: size).image { _ in
                   draw(in: CGRect(origin: .zero, size: size))
               }
    }
}
