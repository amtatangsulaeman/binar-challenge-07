//
//  UploadImageViewController.swift
//  Challenge 7
//
//  Created by Tatang Sulaeman on 04/08/22.
//

import UIKit

class UploadImageViewController: UIViewController,UIImagePickerControllerDelegate,UINavigationControllerDelegate {

    weak var delegate:HomeViewController?
    var image: UIImage = UIImage()
    lazy var addPhotoButton: UIButton = {
        let button = UIButton()
        let Config = UIImage.SymbolConfiguration(pointSize: 175, weight: .bold, scale: .large)
        let photoSymbol = UIImage(systemName: "photo.fill", withConfiguration: Config)
        button.setImage(photoSymbol, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var addImageButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Choose Image  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(addPhotoAction), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Save Image  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(savePhotoAction), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var addFileStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [addImageButton,UIView(),saveButton])
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    @objc func addPhotoAction(_ sender: Any) {
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.sourceType = .photoLibrary
        imagePickerVC.delegate = self // new
        present(imagePickerVC, animated: true)
    }
    
    @objc func savePhotoAction(_ sender: Any) {
        delegate?.image = image
        self.navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)

        if let image = info[.originalImage] as? UIImage {
            self.image = image
            addPhotoButton.setImage(image.resizeImageTo(size: CGSize(width:self.view.frame.size.width, height:300) ), for: .normal)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(addPhotoButton)
        view.addSubview(addFileStack)
        
        NSLayoutConstraint.activate([
            
            addPhotoButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            addPhotoButton.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -20),
            
            addFileStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            addFileStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            addFileStack.topAnchor.constraint(equalTo: addPhotoButton.bottomAnchor, constant: 20),
        
    
        ])
        
    }
}
