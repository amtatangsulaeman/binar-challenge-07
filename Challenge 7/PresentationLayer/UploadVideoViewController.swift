//
//  UploadVideoViewController.swift
//  Challenge 7
//
//  Created by Tatang Sulaeman on 04/08/22.
//

import UIKit
import AVFoundation
class UploadVideoViewController: UIViewController,UINavigationControllerDelegate,UIImagePickerControllerDelegate {

    weak var delegate:HomeViewController?
    var videoURL: URL = URL(fileURLWithPath: "")
    lazy var videoView: UIButton = {
        let button = UIButton()
        let Config = UIImage.SymbolConfiguration(pointSize: 175, weight: .bold, scale: .large)
        let photoSymbol = UIImage(systemName: "photo.fill", withConfiguration: Config)
        button.setImage(photoSymbol, for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var addVideoButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Choose video  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(addVideoAction), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private lazy var saveButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Save video  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(saveVideoAction), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    lazy var addFileStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [addVideoButton,UIView(),saveButton])
        stackView.axis = .horizontal
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
    }()
    
    @objc func addVideoAction(_ sender: Any) {
        let imagePickerVC = UIImagePickerController()
        imagePickerVC.sourceType = .photoLibrary
        imagePickerVC.delegate = self // new
        imagePickerVC.sourceType = .photoLibrary
        imagePickerVC.mediaTypes = [UTType.movie.identifier as String]
        present(imagePickerVC, animated: true)
    }
    
    @objc func saveVideoAction(_ sender: Any) {
        delegate?.videoURL = videoURL
        self.navigationController?.popViewController(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let videoURL = info[UIImagePickerController.InfoKey.mediaURL] as? URL else { return }
        self.videoURL = videoURL
        let player = AVPlayer(url: videoURL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.frame = self.view.bounds
        playerLayer.position = CGPoint(x:videoView.bounds.midX,y:videoView.bounds.midY)
        self.videoView.layer.addSublayer(playerLayer)
        player.play()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        view.addSubview(videoView)
        view.addSubview(addFileStack)
        
        NSLayoutConstraint.activate([
            
            videoView.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            videoView.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -20),
            
            addFileStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            addFileStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            addFileStack.topAnchor.constraint(equalTo: videoView.bottomAnchor, constant: 20),
        
    
        ])
        
    }

}
