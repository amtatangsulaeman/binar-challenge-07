//
//  ReviewModel.swift
//  Challenge 7
//
//  Created by Tatang Sulaeman on 04/08/22.
//

import Foundation

struct ReviewModel {
    let name: String
    let rating: String
    let desc: String
    let favMenu: String
    let image: Data
    let video: Data
}
