//
//  ReviewViewController.swift
//  Challenge 7
//
//  Created by Tatang Sulaeman on 04/08/22.
//

import UIKit
import CoreData
import AVFoundation

class ReviewViewController: UIViewController {

    var reviews: [ReviewModel] = []
    func createFormItem(name: String, longValue: Bool = false) -> UIStackView{
        lazy var label: UILabel = {
            let lbl = UILabel()
            lbl.text = name
            lbl.textColor = .black
            lbl.font = UIFont(name:"HelveticaNeue-bold", size: 15)
            return lbl
        }()
        
        lazy var textView: UITextView = {
            let txtView = UITextView()
            txtView.layer.cornerRadius = 5
            txtView.layer.borderColor = UIColor.systemGray.cgColor
            txtView.layer.borderWidth = 1
            txtView.font = UIFont.systemFont(ofSize: 15)
            if (longValue == true){
                txtView.addConstraint(txtView.heightAnchor.constraint(equalToConstant: 100))
            }
            else{
                txtView.addConstraint(txtView.heightAnchor.constraint(equalToConstant: 40))
            }
            txtView.isEditable = false
            return txtView
        }()
                               
        
        lazy var formItemStack: UIStackView = {
            let stackView = UIStackView()
            stackView.addArrangedSubview(label)
            stackView.addArrangedSubview(textView)
            stackView.axis = .vertical
            stackView.translatesAutoresizingMaskIntoConstraints = false
            return stackView
        }()
        
        
        return formItemStack
    }
    
    lazy var nameFormStack: UIStackView = createFormItem(name:"Resto name")
    lazy var ratingFormStack: UIStackView = createFormItem(name:"Rating")
    lazy var descFormStack: UIStackView = createFormItem(name:"Description", longValue: true)
    lazy var favMenuFormStack: UIStackView = createFormItem(name:"Favorite Menu")
    
    lazy var formStack: UIStackView = {
        let stackView = UIStackView(arrangedSubviews: [
            nameFormStack,
            ratingFormStack,
            descFormStack,
            favMenuFormStack
        ])
        stackView.spacing = 10
        stackView.axis = .vertical
        stackView.translatesAutoresizingMaskIntoConstraints = false
        return stackView
     }()
    
    lazy var container1View: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    lazy var container2View: UIView = {
        let view = UIView()
        view.frame.size.height = 200
        view.frame.size.width = 200
        view.backgroundColor = .blue
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    func getData(){
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        
        let managedContext = appDelegate.persistentContainer.viewContext
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "ReviewEntity")
        
        do {
            let result = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            print(result.count)
            guard result.count != 0 else {return}
            reviews = []
            result.forEach { review in
                reviews.append(
                    ReviewModel(
                        name: review.value(forKey: "name") as! String,
                        rating: review.value(forKey: "rating") as! String,
                        desc: review.value(forKey: "desc") as! String,
                        favMenu: review.value(forKey: "fav_menu") as! String,
                        image: review.value(forKey: "image") as? Data ?? Data(),
                        video: review.value(forKey: "video") as? Data ?? Data()
                    )
                )
            }
        } catch let err{
            print(err)
        }
        
        setData(data: reviews[reviews.count-1])
    }
    
    func setData(data: ReviewModel){
        let fieldName = nameFormStack.arrangedSubviews[1] as! UITextView
        let fieldRating = ratingFormStack.arrangedSubviews[1] as! UITextView
        let fieldDesc = descFormStack.arrangedSubviews[1] as! UITextView
        let fieldFavMenu = favMenuFormStack.arrangedSubviews[1] as! UITextView
        let imageContainer = UIImageView()
        
        fieldName.text = data.name
        fieldRating.text = data.rating
        fieldDesc.text = data.desc
        fieldFavMenu.text = data.favMenu
        
        print(data.image," - ",data.video)


        
        if let dataImage = UIImage(data: data.image){
            imageContainer.image = dataImage
            container1View.addSubview(imageContainer, stretchToFit: true)
        }
        
        
        let dataVideo = data.video
        let cacheURL = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!.appendingPathComponent("yourvidename.mp4")

        do {
            try dataVideo.write(to: cacheURL, options: .atomicWrite)
        } catch let err {
            print("Failed with error:", err)
        }

        let player = AVPlayer(url: cacheURL)
        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        container2View.setNeedsLayout()
        container2View.layoutIfNeeded()
        playerLayer.frame = container2View.bounds
        print(container2View.bounds," - ",container2View.frame)
        container2View.layer.addSublayer(playerLayer)
        player.play()

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Home"
    
        view.backgroundColor = .white
        setupLayout()
        getData()
    }
    
    func setupLayout(){
        view.addSubview(formStack)
        view.addSubview(container1View)
        view.addSubview(container2View)
        NSLayoutConstraint.activate([
            formStack.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20),
            formStack.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -20),
            formStack.topAnchor.constraint(equalTo: view.topAnchor,constant: 100),
            
            container1View.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            container1View.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            container1View.topAnchor.constraint(equalTo: formStack.bottomAnchor,constant: 20),
            container1View.heightAnchor.constraint(equalToConstant: 200),
            
            container2View.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50),
            container2View.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -50),
            container2View.topAnchor.constraint(equalTo: container1View.bottomAnchor,constant: 20),
            container2View.heightAnchor.constraint(equalToConstant: 200),

        ])
        
    }
}
