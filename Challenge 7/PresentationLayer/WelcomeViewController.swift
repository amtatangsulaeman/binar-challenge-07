//
//  WelcomeViewController.swift
//  Challenge 7
//
//  Created by Tatang Sulaeman on 04/08/22.
//

import UIKit

class WelcomeViewController: UIViewController {

    lazy var titleLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Resto Review"
        lbl.textColor = .black
        lbl.font = UIFont(name:"HelveticaNeue-Bold", size: 34)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    lazy var welcomeLabel: UILabel = {
        let lbl = UILabel()
        lbl.text = "Welcome to"
        lbl.textColor = .systemBlue
        lbl.font = UIFont(name:"HelveticaNeue", size: 15)
        lbl.translatesAutoresizingMaskIntoConstraints = false
        return lbl
    }()
    
    private lazy var continueButton: UIButton = {
        let button = UIButton()
        button.setTitle("  Continue  ", for: .normal)
        button.layer.cornerRadius = 5
        button.backgroundColor = .systemBlue
        button.addTarget(self, action: #selector(navigateToHome), for: .touchUpInside)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    @objc func navigateToHome(){
        let homeVC = HomeViewController()
        homeVC.navigationItem.hidesBackButton = true
        self.navigationController?.pushViewController(homeVC, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        setupLayout()
        
    }
    
    func setupLayout(){
        view.addSubview(titleLabel)
        view.addSubview(welcomeLabel)
        view.addSubview(continueButton)
        
        NSLayoutConstraint.activate([
            
            titleLabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
            titleLabel.centerYAnchor.constraint(equalTo: self.view.centerYAnchor, constant: -20),
            
            welcomeLabel.bottomAnchor.constraint(equalTo: titleLabel.topAnchor, constant: 5),
            welcomeLabel.leadingAnchor.constraint(equalTo: titleLabel.leadingAnchor),
            
            continueButton.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 20),
            continueButton.centerXAnchor.constraint(equalTo: self.view.centerXAnchor),
        
    
        ])
    }

}
